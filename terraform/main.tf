provider "aws" {
    region = "ap-northeast-1"  
}

resource "aws_instance" "webserver" {
    ami                    = "ami-072bfb8ae2c884cc4"
    instance_type          = "t2.micro"
    vpc_security_group_ids = [aws_security_group.webserver.id]
    key_name = "aws_test"
    tags = {
        Name = "Webserver"
    }
}

resource "aws_security_group" "webserver" {
    name = "Webserver security group"

    dynamic "ingress" {
        for_each = ["80", "443"]
        content {
            from_port   = ingress.value
            to_port     = ingress.value
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"] 
        }      
    }

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "Webserver"
    }
}
