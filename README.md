### Marat Biryushev

## Application
The flask_app directory contains an application that returns "Hello world! Time is: TIME_IN_MOSCOW" when accessing it. Dockerfile, with main image: python: 3. 
- the flask user is created, 
- requirements.txt is transferred to the container, 
- dependencies are installed, 
- the code is transferred to the container, the root is changed to the "flask" user,
 - the application starts.

## Infrastructure
The terraform directory describes the infrastructure for deploying EC2 instance on Amazon Web Services. The provider is "aws". 
- Creates EC2 instance (Amazon Machine Image)
- Creates security group to open ports 443, 80, 22.
- Adds key pair to conect to instance from ansible server.

## Bootstrapping
The ansible directory contains a playbook.yml that uses 3 roles:
Docker role
- Installs the latest version of docker on the EC2  instance.

App role
- Moves the application code and Dockerfile to the target virtual machine.
- Builds the app image and launches the app container on port 5000.

Nginx role
- Installs nginx on the EC2 instance.
- Adds self signed certificate and key. 
- Starts nginx as proxy server
