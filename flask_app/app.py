from datetime import datetime
from pytz import timezone
from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html', datetime=str(datetime.now(timezone('Europe/Moscow')).strftime('%H-%M-%S')), title="Test_page")
    #return "Hello "


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
